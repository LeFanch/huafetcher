# huafetcher

Kivy GUI for huami-token. Works on desktop and as Android apk. Downloads key
and aGPS data, unzips it into `/storage/emulated/0`

[huami-token](https://github.com/argrento/huami-token): all credits to the original author

To be able to get a key from huami servers, the band/watch must be paired with
the original app at least once. Follow instructions in [Gadgetbridge
wiki](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Huami-Server-Pairing#server-based-pairing).

<img src="screenshots/screenshot1.jpg" width="300" alt="screenshot" />
<img src="screenshots/screenshot2.jpg" width="300" alt="screenshot" />

## Developer

### Install

Install [Buildozer](https://github.com/kivy/buildozer/) and [Kivy](https://github.com/kivy/kivy)

pip install buildozer
pip install kivy

or

pip install -r requirements.txt

### Run

python main.py

### Make Android apk

buildozer -v android debug deploy run

## User

### Obtaining apk

Either build apk as per above or get it from the [releases](https://codeberg.org/vanous/huafetcher/releases)

### Signing into account

This process for Xiaomi login is pretty unintuitive, so make sure to read this first!

1) Install the apk, run it and manually give it permission for storage (only needed if you plan to fetch aGPS).

<img src="screenshots/01a.jpg" width="300" alt="screenshot" />
<img src="screenshots/01b.jpg" width="300" alt="screenshot" />

## Amazfit

- Choose Amazfit as login method. 
- Fill in email and name fields. You can save them in the app by pressing the Save buttons. 

<img src="screenshots/amazfit_screen.png" width="300" alt="screenshot" />

Note: saved data is not protected. If you use this to get aGPS data periodically, create a throwaway account without any devices.

### Key

- For key, press `Fetch key`. The key will appear in the `Found key` field, get it by pressing the Copy button.

Note: Only last key provided by the server is displayed.

<img src="screenshots/07.jpg" width="300" alt="screenshot" />

This key must be entered into Gadgetbridge during pairing process with devices that require it:

<img src="screenshots/12.jpeg" width="300" alt="screenshot" />
<img src="screenshots/13.jpeg" width="300" alt="screenshot" />
<img src="screenshots/14.jpeg" width="300" alt="screenshot" />

### aGPS

- For aGPS, press `Fetch aGPS` button. Files will be downloaded and the two important files (`cep_pak.bin` and `gps_alm.bin`) will be unzipped into `/storage/emulated/0`. You can send them into your device with the Gadgetbridge FW/App Installer 

<img src="screenshots/08.jpg" width="300" alt="screenshot" />
<img src="screenshots/09.jpg" width="300" alt="screenshot" />
<img src="screenshots/10.jpg" width="300" alt="screenshot" />
<img src="screenshots/11.jpg" width="300" alt="screenshot" />

## Xiaomi

- Choose Xiaomi as login method. Press `Get token` button. 

<img src="screenshots/01.jpg" width="300" alt="screenshot" />

- This will open up your browser. Here you need to sing into your Xiaomi account.

<img src="screenshots/02.jpg" width="300" alt="screenshot" />

- Once you press Sign in, your browser will either warn you or this gets you to a blank page. In any case, the url starts with `hm.xiaomi.com/watch.do?...`. You must copy this URL:

<img src="screenshots/03.jpg" width="300" alt="screenshot" />
<img src="screenshots/05.jpg" width="300" alt="screenshot" />

- Go back to Huafetcher and paste the URL into the URL result field by using the Paste button.

<img src="screenshots/06.jpg" width="300" alt="screenshot" />

- Now you can get a key or aGPS. 

### Key

- For key, press `Fetch key`. The key will appear in the `Found key` field, get it by pressing the Copy button.

Note: Only last key provided by the server is displayed.

<img src="screenshots/07.jpg" width="300" alt="screenshot" />

This key must be entered into Gadgetbridge during pairing process with devices that require it

<img src="screenshots/12.jpeg" width="300" alt="screenshot" />
<img src="screenshots/13.jpeg" width="300" alt="screenshot" />
<img src="screenshots/14.jpeg" width="300" alt="screenshot" />


### aGPS

- For aGPS, press `Fetch aGPS` button. Files will be downloaded and the two important files (`cep_pak.bin` and `gps_alm.bin`) will be unzipped into `/storage/emulated/0`. You can send them into your device with the Gadgetbridge FW/App Installer 

<img src="screenshots/08.jpg" width="300" alt="screenshot" />
<img src="screenshots/09.jpg" width="300" alt="screenshot" />
<img src="screenshots/10.jpg" width="300" alt="screenshot" />
<img src="screenshots/11.jpg" width="300" alt="screenshot" />

